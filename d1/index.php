<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Introduction</title>
</head>
<body>

<h1>PHP Introductions</h1>

<?php 


    

    $first_name = "Mark Anthony";
    $last_name = "Vivar";
    
    define('PI', 3.1416);
    
    echo "$first_name $last_name";

    echo "<br>";

    $songs = ["Gitara", "Diwata", "Chinita"];
    echo "<h1>$songs[0]</h1>";

    
   

    echo "The total songs you have is" . " " . count($songs);
    
    echo "<br>";
    echo "<br>";

    $finalGrades = (object)[
    "firstGrading" => 98.7,
    "secondGrading" => 92.1,
    "thirdGrading" => 83.3,
    "fourthGrading" => 87.7,
    ];


    echo $finalGrades -> firstGrading;
 


    $personDetails = (object)[
        "fullName" => "Mark Anthony Vivar",
        "favorites" => (object)[
            "movie" => "One Piece",
            "song" => "Gitara"
        ]
    ];


    echo "<br>";
    echo "<br>";

    echo $personDetails -> favorites -> movie;

    echo "<br>";
    echo "<br>";

    $personDetails -> age = 27;
    // var_dump($personDetails);

    // echo $personDetails -> age;


    // echo $personDetails -> favorites;

    $num1 = 5;
    $num2 = 3;

    $result = $num1 / $num2;
    echo number_format($result, 2);


    echo "<br>";
    echo "<br>";


    echo pow(2,3);


    
    echo "<br>";
    echo "<br>";


    echo 5 ** 5;
    

    


?>

</body>
</html>